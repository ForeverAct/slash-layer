import {createVNode, defineComponent, inject, onMounted, PropType, Ref, ref, toRefs, watch} from 'vue'
import {DefineComponent, VNode} from "@vue/runtime-core";

export default defineComponent({
        render() {
            const content: DefineComponent = this.content as DefineComponent;
            let props = this.contentProps;
            const vNode: VNode = createVNode(content, {
                ref: "targetRef",
                ...props,
            }, this.$slots.default?.(),);
            return vNode;
        },
        props: {
            content: {
                type: Object as PropType<DefineComponent>
            },
        },
        setup(p, ctx) {
            const {content} = toRefs(p);
            const targetRef = ref<InstanceType<any>>();
            /** 引入参数 */
            const contentProps = inject("contentProps") as Ref<any>;
            const doSubmit = () => {
                if (targetRef.value.doSubmit) {
                    return targetRef.value.doSubmit();
                }
            }
            const doUpdate = () => {
                if (targetRef.value.doUpdate) {
                    return targetRef.value.doUpdate();
                }
            }
            return {
                content,
                doSubmit,
                targetRef,
                doUpdate,
                contentProps
            }
        }
    }
)
