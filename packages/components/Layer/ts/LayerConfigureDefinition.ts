import {PropType} from "vue";
import {PictureType} from "~/components/LayerImages/DeclareImages";
import {DefineComponent} from "@vue/runtime-core";

export interface LayerPosition {
    width: number,
    height: number,
    top?: number,
    left?: number
}
/** 弹窗类型 */
export type LayerType = 'modal' | 'drawer'
/** 弹窗预置尺寸 */
export type DefaultPosition = 'sm' | 'md' | 'lg'
/** 抽屉位置 */
export type DrawerPlacement = 'top' | 'right' | 'bottom' | 'left'

export interface LayerCache {
    full: boolean,
    position: LayerPosition,
    sWinPosition: LayerPosition
}

export interface Btn {
    title: {}
}

export interface LayerGlobalConfigure {
    title: string,
    max: boolean,
    min: boolean,
    header: boolean,
    /** true：根据config渲染；OptionsContent：根据插槽传入渲染 */
    footer: boolean | OptionsContent,
    areaDef?: object | null,
    yesFunc?: string | null,
    noFunc?: string | null,
    /*各种提示框自动关闭时间*/
    autoCloseTime: number
    /*加载效果时间*/
    loadingTime: number,
    /*主题*/
    theme?: string
    /*遮罩*/
    mask?: boolean,
    //双击全屏
    dbFull?: boolean,
    loadingText?: string,
    allowMove: boolean,
    autoInfo?: boolean,
    /** 抽屉位置预置 */
    placementDef?: object | null,
    /** 组件字体 */
    fontSizeConfig?: object,
    /** 尺寸系数 */
    fontSizeCoefficient?: number,

    successDecide(msg: any): SuccessDecideResult

    baseComponent?: DefineComponent | any | object,
    info: (msg: any, config: OpenConfigure) => void,
    success: (msg: any, config: OpenConfigure) => void,
    error: (msg: any, config: OpenConfigure) => void,
    confirm: (msg: any, config: ConfirmConfigure) => Promise<any>,
    warning: (msg: any, config: OpenConfigure) => void
}

export interface LayerConfigureDefinition {
    title: string,
    max: boolean,
    min: boolean,
    autoInfo?: boolean,
    yesFunc?: string | null,
    noFunc?: string | null,
}

export interface SuccessDecideResult {
    result: boolean,
    msg: string,
    data?: object
}

export interface OptionsContent {
    component: DefineComponent | any | object,
    props?: PropType<any> | any,
    parent?: any
}


export interface OpenConfigure extends LayerConfigureDefinition {
    id?: string | undefined,
    position?: LayerPosition | DefaultPosition | any,
    content: OptionsContent | null,
    header: boolean,
    allowMove: boolean,
    footer: boolean | OptionsContent,
    btn: Array<OpenBtn>
    autoCloseTime: number
    runMode?: string,
    className?: string | undefined
    loadingTime?: number
    theme?: string,
    mask?: boolean,
    loadingText?: string,
    dbFull?: boolean,
    baseComponent?: DefineComponent | any | object,
    /** 弹窗类型： 模态框 | 抽屉 */
    layerType?: LayerType,
    /** 抽屉位置 */
    drawerPlacement?: DrawerPlacement,
    /** 层级 */
    zIndex?: number

    closeCallBack(id?: string, data?: any): string
}

export interface OpenBtn {
    name: string,
    className: string,
    data: any,
    loading: boolean,
    loadingText: string,
    curLoading?: boolean | null

    callback?(instance: any, data?: any): string;
    /** 自定义点击事件处理函数
     * @param contentInstance 弹窗内容对象
     * @param layerInstance 弹窗自身对象
     */
    onClick?(contentInstance: any, layerInstance: any): any
}

export interface ImagesConfigure {
    imgList: Array<PictureType>
}

export interface MessageConfigure {
    title?: string,
    icon?: string,
    iconColor?: string,
    msg: string
}

export interface ConfirmConfigure {
    title?: string,
    icon?: string,
    msg: string,
    data?: any,
    position?: LayerPosition | DefaultPosition,
}

export interface FormConfigure {
    id?: string | undefined,
    title?: string,
    position?: LayerPosition | DefaultPosition,
    content: OptionsContent,
    header?: boolean,
    footer?: boolean | OptionsContent,
    btn?: Array<OpenBtn> | undefined
    autoCloseTime?: number
    runMode?: string,
    className?: string | undefined
    loadingTime?: number
    theme?: string,
    mask?: boolean,
    autoInfo?: boolean,
    /** 弹窗类型： 模态框 | 抽屉 */
    layerType?: LayerType,
    /** 抽屉位置 */
    drawerPlacement?: DrawerPlacement,
    zIndex?: number
}

export interface SelectFileConfig {
    accept: Array<string>[],
}
