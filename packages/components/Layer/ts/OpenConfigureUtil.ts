import {DrawerPlacement, LayerGlobalConfigure, LayerPosition} from "./LayerConfigureDefinition";
import {defaultLayerGlobalConfigure, layer_preset_area_default, layer_preset_area_full, layer_preset_drawer_placement} from "../consts/LayerConst";
import LayerUtil from "../ts/LayerUtil";

export class OpenConfigureUtil {
    /**
     * 获取位置
     */
    public static getOpenPosition(position: LayerPosition | string | undefined, globalConfigure: LayerGlobalConfigure): LayerPosition {
        if (!globalConfigure) {
            globalConfigure = defaultLayerGlobalConfigure;
        }
        console.log("当前定位：{}", position)
        let oldPosition: string | unknown = position;
        const areaDef = globalConfigure.areaDef as any;
        if (typeof position === 'undefined' && !areaDef) {
            console.error("未指定位置和预设位置");
        }
        let tempResultPosition: LayerPosition = {} as LayerPosition;
        if (typeof position === 'undefined') {
            tempResultPosition = areaDef[layer_preset_area_default] as LayerPosition;
        } else if (typeof position === "string") {
            if (areaDef[position]) {
                tempResultPosition = areaDef[position] as LayerPosition;
            } else if (position === layer_preset_area_full) {
                tempResultPosition = {
                    width: LayerUtil.getViewPortWidth(),
                    height: LayerUtil.getViewPortHeight(),
                    top: 0,
                    left: 0
                } as LayerPosition
            }
        } else if (typeof position === "object") {
            tempResultPosition = LayerUtil.deepClone(position) as unknown as LayerPosition;
        }
        if (Object.keys(tempResultPosition).length == 0) {
            console.warn("未指定区域大小")
        }
        if (typeof tempResultPosition.width === 'undefined') {
            tempResultPosition.width = areaDef[layer_preset_area_default].width;
        }
        if (typeof tempResultPosition.height === 'undefined') {
            tempResultPosition.height = areaDef[layer_preset_area_default].height;
        }
        // if (typeof tempResultPosition.top === "undefined") {
        //     tempResultPosition.top = areaDef[layer_preset_area_default].top;
        // }
        let resultPosition: LayerPosition = LayerUtil.deepClone(tempResultPosition) as unknown as LayerPosition;
        if (oldPosition !== "full") {
            resultPosition.width = tempResultPosition.width / 1920.0 * window.screen.width;
            resultPosition.height = tempResultPosition.height / 1080.0 * window.screen.height;
            resultPosition.top = (typeof tempResultPosition.top === "undefined") ? (window.innerHeight - resultPosition.height) / 2 : tempResultPosition.top / 1920.0 * window.screen.height;
            resultPosition.left = (window.innerWidth - resultPosition.width) / 2;
        }
        resultPosition.left = OpenConfigureUtil.getRelativeLeft(resultPosition.width);
        console.log("定位:", resultPosition);
        return resultPosition;
    }


    public static getRelativeLeft(width: number): number {
        return LayerUtil.getViewPortWidth() / 2 - width / 2
    }

    /** 处理抽屉的大小/位置 */
    public static getDrawerPlacement(placement: DrawerPlacement | undefined, position: LayerPosition | string | undefined, globalConfigure: LayerGlobalConfigure): LayerPosition {
        if (!globalConfigure) {
            globalConfigure = defaultLayerGlobalConfigure;
        }
        if (!placement) {
            placement = layer_preset_drawer_placement;
        }
        const placementDef = globalConfigure.placementDef as any;
        if (typeof position === 'undefined' && !placementDef[placement]) {
            console.error("未指定位置和预设位置");
        }
        let tempResultPosition: LayerPosition = {} as LayerPosition;
        if (typeof position === 'undefined') {
            tempResultPosition = placementDef[placement][layer_preset_area_default] as LayerPosition;
        } else if (typeof position === "string") {
            if (placementDef[placement][position]) {
                tempResultPosition = placementDef[placement][position] as LayerPosition;
            } else if (position === layer_preset_area_full) {
                tempResultPosition = {
                    width: LayerUtil.getViewPortWidth(),
                    height: LayerUtil.getViewPortHeight(),
                } as LayerPosition
            }
        } else if (typeof position === "object") {
            // tempResultPosition = LayerUtil.deepClone(position) as unknown as LayerPosition;
            if (['left', 'right'].includes(placement)) {
                tempResultPosition.width = typeof position.width === 'undefined' ?
                    placementDef[placement][layer_preset_area_default].width :
                    position.width
            } else if (['top', 'bottom'].includes(placement)) {
                tempResultPosition.height = typeof position.height === 'undefined' ?
                    placementDef[placement][layer_preset_area_default].height :
                    position.height
            }
        }
        if (Object.keys(tempResultPosition).length == 0) {
            console.warn("未指定区域大小")
        }
        let resultPosition: LayerPosition = LayerUtil.deepClone(tempResultPosition) as unknown as LayerPosition;
        if (position !== "full") {
            resultPosition.width && (resultPosition.width = tempResultPosition.width / 1920.0 * window.screen.width);
            resultPosition.height && (resultPosition.height = tempResultPosition.height / 1080.0 * window.screen.height);
        }
        console.log("定位:", resultPosition);
        return resultPosition;
    }

}

export default OpenConfigureUtil
