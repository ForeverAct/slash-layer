/**
 *
 */
import {DrawerPlacement, LayerGlobalConfigure, LayerPosition, SuccessDecideResult} from "../ts/LayerConfigureDefinition";

export const layer_id_prefix: string = "layer_"
export const layer_root_prefix: string = "root_"

/**
 *
 */
export const layer_mask_prefix: string = "mask"

/**
 * 进入动画映射
 */
export const fadeinAnimationMap = {
    modal: "slash-top-fadein",
    drawer: {
        top: "slash-top-fadein",
        left: "slash-left-fadein",
        right: "slash-right-fadein",
        bottom: "slash-bottom-fadein"
    }
}
/**
 * 离开动画映射
 */
export const fadeoutAnimationMap: Record<string, any> = {
    modal: "slash-top-fadeout",
    drawer: {
        top: "slash-top-fadeout",
        left: "slash-left-fadeout",
        right: "slash-right-fadeout",
        bottom: "slash-bottom-fadeout"
    }
}

export const layer_preset_area_sm: string = "sm";
export const layer_preset_area_md: string = "md";
export const layer_preset_area_lg: string = "lg";
export const layer_preset_area_default: string = "default";
export const layer_preset_area_full: string = "full"
export const layer_preset_drawer_placement: DrawerPlacement = "right";

export const defaultLayerGlobalConfigure = {
    title: "弹出框",
    max: true,
    min: true,
    header: true,
    footer: true,
    autoCloseTime: 0,
    loadingTime: 200,
    successDecide: (msg: any) => {
        console.error("请配置successDecide才能使用表单弹框")
    },
    areaDef: {
        "sm": {
            width: 400,
            height: 600,
            // top: 120
            top: undefined
        },
        "md": {
            width: 800,
            height: 600,
            // top: 100
            top: undefined
        },
        "lg": {
            width: 1200,
            height: 800,
            // top: 60
            top: undefined
        },
        "default": {
            width: 800,
            height: 600,
            // top: 120
            top: undefined
        }
    },
    /** 抽屉位置预置 */
    placementDef: {
        top: {
            "sm": {
                height: 150
            },
            "md": {
                height: 250
            },
            "lg": {
                height: 350
            },
            "default": {
                height: 250
            }
        },
        bottom: {
            "sm": {
                height: 150
            },
            "md": {
                height: 250
            },
            "lg": {
                height: 350
            },
            "default": {
                height: 250
            }
        },
        left: {
            "sm": {
                width: 250
            },
            "md": {
                width: 350
            },
            "lg": {
                width: 500
            },
            "default": {
                width: 350
            }
        },
        right: {
            "sm": {
                width: 250
            },
            "md": {
                width: 350
            },
            "lg": {
                width: 500
            },
            "default": {
                width: 350
            }
        }
    }
} as LayerGlobalConfigure