export const fontThemes = {
  "common": {
      "fontSize": "16.8px",
      "baseColor": "#FFFFFF",
      "primaryColor": "#0f93ff",
      "primaryColorHover": "#0f93ff",
      "primaryColorPressed": "#0f93ff"
  },
  "LoadingBar": {
      "colorLoading": "#0084f4"
  },
  "Alert": {
      "fontSize": "16.8px"
  },
  "Button": {
      "fontSizeTiny": "14.399999999999999px",
      "fontSizeSmall": "16.8px",
      "fontSizeMedium": "16.8px",
      "fontSizeLarge": "18px"
  },
  "Calendar": {
      "titleFontSize": "26.4px",
      "fontSize": "16.8px"
  },
  "Card": {
      "titleFontSizeSmall": "19.2px",
      "titleFontSizeMedium": "21.599999999999998px",
      "titleFontSizeLarge": "21.599999999999998px",
      "titleFontSizeHuge": "21.599999999999998px",
      "fontSizeSmall": "16.8px",
      "fontSizeMedium": "16.8px",
      "fontSizeLarge": "16.8px",
      "fontSizeHuge": "16.8px"
  },
  "Carousel": {},
  "Cascader": {
      "optionFontSize": "16.8px"
  },
  "Checkbox": {
      "sizeSmall": "16.8px",
      "sizeMedium": "19.2px",
      "sizeLarge": "21.599999999999998px",
      "fontSizeSmall": "16.8px",
      "fontSizeMedium": "16.8px",
      "fontSizeLarge": "18px"
  },
  "Code": {},
  "Collapse": {
      "titleFontSize": "16.8px",
      "fontSize": "16.8px"
  },
  "ColorPicker": {
      "panelFontSize": "16.8px",
      "fontSizeSmall": "16.8px",
      "fontSizeMedium": "16.8px",
      "fontSizeLarge": "18px"
  },
  "DataTable": {
      "fontSizeSmall": "16.8px",
      "fontSizeMedium": "16.8px",
      "fontSizeLarge": "18px"
  },
  "DatePicker": {
      "itemFontSize": "16.8px"
  },
  "Descriptions": {
      "fontSizeSmall": "16.8px",
      "fontSizeMedium": "16.8px",
      "fontSizeLarge": "18px"
  },
  "Dialog": {
      "titleFontSize": "21.599999999999998px"
  },
  "Drawer": {},
  "Dropdown": {
      "fontSizeSmall": "16.8px",
      "fontSizeMedium": "16.8px",
      "fontSizeLarge": "18px",
      "fontSizeHuge": "19.2px"
  },
  "Empty": {
      "fontSizeSmall": "16.8px",
      "fontSizeMedium": "16.8px",
      "fontSizeLarge": "18px",
      "fontSizeHuge": "19.2px"
  },
  "Form": {
      "labelFontSizeLeftSmall": "16.8px",
      "labelFontSizeLeftMedium": "16.8px",
      "labelFontSizeLeftLarge": "18px",
      "labelFontSizeTopSmall": "15.6px",
      "labelFontSizeTopMedium": "16.8px",
      "labelFontSizeTopLarge": "16.8px"
  },
  "GradientText": {},
  "Input": {
      "fontSizeTiny": "14.399999999999999px",
      "fontSizeSmall": "16.8px",
      "fontSizeMedium": "16.8px",
      "fontSizeLarge": "18px"
  },
  "List": {
      "fontSize": "16.8px"
  },
  "Menu": {
      "fontSize": "16.8px"
  },
  "Message": {
      "fontSize": "16.8px"
  },
  "Notification": {},
  "Pagination": {
      "itemFontSizeSmall": "14.399999999999999px",
      "itemFontSizeMedium": "16.8px",
      "itemFontSizeLarge": "16.8px",
      "jumperFontSizeSmall": "14.399999999999999px",
      "jumperFontSizeMedium": "16.8px",
      "jumperFontSizeLarge": "16.8px"
  }
};