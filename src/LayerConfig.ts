import {LayerGlobalConfigure} from "../packages/components/Layer/ts/LayerConfigureDefinition";
import {ElMessage, ElMessageBox} from 'element-plus'

export const config = {
    title: "自定义全局标题",
    max: true,
    min: true,
    header: true,
    mask: true,
    loadingTime: 1000,
    dbFull: true,
    allowMove: true,
    theme: "layer-theme",
    fontSizeConfig: {},
    fontSizeCoefficient: 1.5,
    // baseComponent:Test,
    successDecide(msg: any) {
        console.log(msg);
        if (msg.code == 200) {
            return {
                msg: msg.msg, result: true, data: msg.data
            }
        } else {
            return {
                msg: msg.msg, result: false, data: msg.data
            }
        }
    },
    success(msg, config) {
        window.$message.info(msg,{
            duration: 0
        });
    },
    info(msg, config) {
        ElMessage({
            message: msg,
            type: 'info',
        })
    },
    error(msg, config) {
        ElMessage({
            message: msg,
            type: 'error',
        })
    },
    confirm(msg, confirm) {
        return ElMessageBox.confirm(
            msg,
            confirm.title,
            {
                confirmButtonText: '确认',
                cancelButtonText: '取消',
                type: 'warning',
            }
        );
    }
} as LayerGlobalConfigure
export default config
