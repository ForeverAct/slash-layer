export const testData = [
  {
    data: {
      user: 'dsr',
      region: 'shanghai',
      date: '2023-07-26',
    }
  },
  {
    data: {
      user: 'test',
      region: 'beijing',
      date: '2023-06-01',
    }
  },
  {
    data: {
      user: 'mymy',
      region: 'shanghai',
      date: '2023-08-10',
    }
  }
]