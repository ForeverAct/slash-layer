<!-- _coverpage.md -->

[comment]: <> (![logo]&#40;_media/icon.svg&#41;)

# SlashLayer <small>1.0</small>

> Vue3轻量弹框插件


- 基于VUE3开发的Web弹出层插件
- 内置各种常用尺寸模态框、提示消息等，可以轻松完成后台管理系统等业务表单开发，全身心投入摸鱼事业!

[GitHub](https://github.com/lanmushan/slash-layer)
[GITEE](https://gitee.com/lanmushan/slash-layer)
